/*
 * A simple transfer method moving the data via the host.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _DGMA_HPP_
#define _DGMA_HPP_

#include <vector>
#include <CL/cl_ext.h>
#include "transfer.hpp"
#include "update.hpp"

class DirectGMA : public Transfer {
public:
	DirectGMA(cl_command_queue src, cl_command_queue dest, size_t bytes);
	virtual ~DirectGMA();
	virtual void operator()(cl_mem src, cl_mem dest) const override;
private:
	/** the cache buffer on the dest device that can be accessed via bus */
	cl_mem bus_addressable_buffer = 0;
	cl_bus_address_amd bus_addressable_buffer_address;
	/** the buffer on the source device, which is actually backed by the buffer bus_visible_buffer */
	cl_mem shadow_buffer = 0;
	// OpenCL extension functions required for DirectGMA
	clEnqueueWaitSignalAMD_fn clEnqueueWaitSignalAMD = nullptr;
	clEnqueueWriteSignalAMD_fn clEnqueueWriteSignalAMD = nullptr;
	clEnqueueMakeBuffersResidentAMD_fn clEnqueueMakeBuffersResidentAMD = nullptr;
};

class DirectGMAUpdate : public Update {
public:
	DirectGMAUpdate(std::vector<cl_command_queue> const & queues, size_t bytes);
	virtual ~DirectGMAUpdate();
	virtual void operator()(std::vector<cl_mem> const & buffers) const override;
private:
	/** the cache buffer on the dest device that can be accessed via bus */
	std::vector<cl_mem> bus_addressable_buffers;
	std::vector<cl_bus_address_amd> bus_addressable_buffer_addresses;
	/** the buffer on the source device, which is actually backed by the buffer bus_visible_buffer */
	std::vector<cl_mem> shadow_buffers;
	// OpenCL extension functions required for DirectGMA
	clEnqueueWaitSignalAMD_fn clEnqueueWaitSignalAMD = nullptr;
	clEnqueueWriteSignalAMD_fn clEnqueueWriteSignalAMD = nullptr;
	clEnqueueMakeBuffersResidentAMD_fn clEnqueueMakeBuffersResidentAMD = nullptr;
};

#endif
