/*
 * A simple transfer method relying on standard opencl methods.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "pinned.hpp"
#include "util.hpp"

Pinned::Pinned(cl_command_queue const src, cl_command_queue const dest, size_t const bytes)
 : Transfer(src, dest, bytes) {

	cl_context context;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
	cl_device_id src_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_DEVICE, sizeof(cl_device_id), &src_device, nullptr));

	cl_int err;
	pinned_buf = clCreateBuffer(context, CL_MEM_ALLOC_HOST_PTR, size, nullptr, &err);
	FAIL_ON_ERR(err);
	pinned_mem = clEnqueueMapBuffer(src_queue, pinned_buf, CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, size, 0, nullptr, nullptr, &err);
	FAIL_ON_ERR(err);
}

Pinned::~Pinned()
{
	clEnqueueUnmapMemObject(src_queue, pinned_buf, pinned_mem, 0, nullptr, nullptr);
	clReleaseMemObject(pinned_buf);
}

void Pinned::operator()(cl_mem const src, cl_mem const dest) const
{
	cl_event to_host, to_dest;

	FAIL_ON_ERR(clEnqueueReadBuffer(src_queue, src, CL_FALSE, 0, size, pinned_mem, 0, nullptr, &to_host));
	FAIL_ON_ERR(clEnqueueWriteBuffer(dest_queue, dest, CL_FALSE, 0, size, pinned_mem, 1, &to_host, &to_dest));
	FAIL_ON_ERR(clEnqueueBarrierWithWaitList(src_queue, 1, &to_dest, nullptr));

	clReleaseEvent(to_host);
	clReleaseEvent(to_dest);
}

PinnedUpdate::PinnedUpdate(std::vector<cl_command_queue> const & queues, size_t const bytes)
 : Update(queues, bytes), pinned_buffers(queues.size()), pinned_mems(queues.size())
{

	size_t const num_queues = queues.size();
	for(size_t i = 0; i < num_queues; ++i) {
		auto const queue = queues[i];
		cl_context context;
		FAIL_ON_ERR(clGetCommandQueueInfo(queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
		cl_device_id device;
		FAIL_ON_ERR(clGetCommandQueueInfo(queue, CL_QUEUE_DEVICE, sizeof(cl_device_id), &device, nullptr));

		cl_int err;
		pinned_buffers[i] = clCreateBuffer(context, CL_MEM_ALLOC_HOST_PTR, size, nullptr, &err);
		FAIL_ON_ERR(err);
		pinned_mems[i] = clEnqueueMapBuffer(queue, pinned_buffers[i], CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, size, 0, nullptr, nullptr, &err);
		FAIL_ON_ERR(err);
	}
}

PinnedUpdate::~PinnedUpdate()
{
	size_t const num_queues = queues.size();
	for(size_t i = 0; i < num_queues; ++i) {
		auto const queue = queues[i];
		auto const buffer = pinned_buffers[i];
		auto const memory = pinned_mems[i];
		clEnqueueUnmapMemObject(queue, buffer, memory, 0, nullptr, nullptr);
		clReleaseMemObject(buffer);
	}
}

void PinnedUpdate::operator()(std::vector<cl_mem> const & buffers) const
{
	size_t const num_queues = queues.size();
	std::vector<cl_event> to_host(num_queues);
	std::vector<cl_event> from_host(num_queues);
	auto const half_size = size / 2;

	for(size_t i = 0; i < num_queues; ++i) {
		FAIL_ON_ERR(clEnqueueReadBuffer(queues[i], buffers[i], CL_FALSE, half_size, half_size, pinned_mems[i], 0, nullptr, &to_host[i]));
	}
	for(size_t i = 0; i < num_queues; ++i) {
		auto const lower = get_lower_neighbour(i, num_queues);
		FAIL_ON_ERR(clEnqueueWriteBuffer(queues[i], buffers[i], CL_FALSE, 0, half_size, pinned_mems[lower], 1, &to_host[lower], &from_host[lower]));
	}

	// block next transfer until done
	for(size_t i = 0; i < num_queues; ++i) {
		FAIL_ON_ERR(clEnqueueBarrierWithWaitList(queues[i], 1, &from_host[i], nullptr));
	}

	for(auto const event: to_host) {
		clReleaseEvent(event);
	}
	for(auto const event: from_host) {
		clReleaseEvent(event);
	}
}
