/*
 * A simple transfer method relying on standard opencl methods.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "buffer_copy.hpp"
#include "util.hpp"
#include <string>
#include <iostream>
#include <vector>
#include <array>

BufferCopy::BufferCopy(cl_command_queue const src, cl_command_queue const dest, size_t const bytes)
 : Transfer(src, dest, bytes) {

	cl_context context;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
	cl_device_id src_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_DEVICE, sizeof(cl_device_id), &src_device, nullptr));
	cl_device_id dest_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(dest, CL_QUEUE_DEVICE, sizeof(cl_device_id), &dest_device, nullptr));

	const std::string kernel_source = "__kernel void dummy(__global float * foo) { }";
	const char * source = { kernel_source.c_str() };
	size_t source_length = kernel_source.size();
	cl_int clerr;
	cl_program program = clCreateProgramWithSource(context, 1, &source, &source_length , &clerr);
	FAIL_ON_ERR(clerr);
	std::array<cl_device_id, 2> devices = {{src_device, dest_device}};
	clerr = clBuildProgram(program, devices.size(), devices.data(), "", nullptr, nullptr);
	size_t logSize;
	clerr = clGetProgramBuildInfo(program, src_device, CL_PROGRAM_BUILD_LOG, 0, nullptr, &logSize);
	if(!clerr && logSize > 1) { // 0-terminated -> always at least one byte
		std::cout << "Build Log:\n";
		std::vector<char> log(logSize);
		clerr = clGetProgramBuildInfo(program, src_device, CL_PROGRAM_BUILD_LOG, log.size(), log.data(), &logSize);
		std::cout << log.data();
	}

	_dummy = clCreateKernel(program, "dummy", &clerr);
	FAIL_ON_ERR(clerr);

	clReleaseProgram(program);

	cl_int err;
	copy_event = clCreateUserEvent(context, &err);
	clSetUserEventStatus(copy_event, CL_COMPLETE);
}

BufferCopy::~BufferCopy()
{
	clReleaseKernel(_dummy);
}

void BufferCopy::operator()(cl_mem const src, cl_mem const dest) const
{
	// make sure src queue is ready
	cl_event touch_event;
	touch(src_queue, src, {copy_event}, &touch_event);
	clFlush(src_queue);
	clReleaseEvent(copy_event);
	FAIL_ON_ERR(clEnqueueCopyBuffer(dest_queue, src, dest, 0, 0, size, 1, &touch_event, &copy_event));
	clFlush(dest_queue);

	clReleaseEvent(touch_event);
}

void BufferCopy::touch(cl_command_queue const queue, cl_mem const buf, std::vector<cl_event> const events, cl_event * event) const {
	FAIL_ON_ERR(clSetKernelArg(_dummy, 0, sizeof(cl_mem), &buf));
	size_t one = 1;
	FAIL_ON_ERR(clEnqueueNDRangeKernel(queue, _dummy, 1, nullptr, &one, &one, events.size(), events.data(), event));
}
