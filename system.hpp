/*
 * Basic system setup for the benchmarks.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _SYSTEM_HPP_
#define _SYSTEM_HPP_

#include <vector>
#include <CL/cl.h>

class System {

public:
	System(unsigned platform, std::vector<unsigned> const & devices, size_t problem_size);
	~System();

	std::vector<cl_command_queue> const & get_queues() const noexcept { return queues; }
	std::vector<cl_mem> const & get_buffers() const noexcept { return buffers; }

private:
	cl_context context;
	std::vector<cl_command_queue> queues;
	std::vector<cl_mem> buffers;
	size_t const problem_size;
};

void sync_on_queues(std::vector<cl_command_queue> const &);

#endif
