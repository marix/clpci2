/*
 * Execution of device to device transfers using a variate
 * of implementations
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
*/

#include <iostream>
#include <iomanip>
#include <chrono>
#include <boost/program_options.hpp>
#include <CL/cl.h>
#include "transfer.hpp"
#include "buffer_copy.hpp"
#include "util.hpp"
#include "system.hpp"

namespace {
	void benchmark(Transfer const * transfer_method, cl_command_queue src_queue, cl_command_queue dest_queue, cl_mem src_buffer, cl_mem dest_buffer, size_t num_transfers);
	bool verify(Transfer const * transfer_method, cl_command_queue src_queue, cl_command_queue dest_queue, cl_mem src_buffer, cl_mem dest_buffer, size_t num_transfers);
}

int main(int ac, const char** av)
{
	namespace po = boost::program_options;

	/*
	 * HANDLE COMMAND LINE
	 */
	po::options_description desc("Allowed Options");
	desc.add_options()
		("help,h", "produce help message")
		("platform,p", po::value<unsigned>()->default_value(0), "Select platform")
		("src", po::value<unsigned>()->required(), "The source device for the transfer.")
		("dest", po::value<unsigned>()->required(), "The source device for the transfer.")
		("size,s", po::value<size_t>()->default_value(4*1024*1024), "Transfer size in bytes")
		("transfers,n", po::value<size_t>()->default_value(1000), "Number of transfers to perform")
		("verify,c", "Verify transfer instead of benchmarking")
		("method,m", po::value<std::string>()->default_value("buffer_copy"), "The copy method to use")
		("sweep", "Sweep through the memory sizes")
	;

	po::positional_options_description p_desc;
	p_desc.add("src", 1);
	p_desc.add("dest", 1);

	po::variables_map args;
	po::store(po::command_line_parser(ac, av).options(desc).positional(p_desc).run(), args);
	po::notify(args);

	if(args.count("help")) {
		std::cout << desc << '\n';
		return 1;
	}

	auto const transfer_size = args["size"].as<size_t>();
	std::cout << "Transfer Size (B):   " << transfer_size << '\n';
	auto const num_transfers = args["transfers"].as<size_t>();
	std::cout << "Number of Transfers: " << num_transfers << '\n';

	System const system(args["platform"].as<unsigned>(), {args["src"].as<unsigned>(), args["dest"].as<unsigned>()}, transfer_size);
	auto const src_queue = system.get_queues()[0];
	auto const dest_queue = system.get_queues()[1];
	auto const src_buffer = system.get_buffers()[0];
	auto const dest_buffer = system.get_buffers()[1];

	/*
	 * Benchmark
	 */
	auto const verification_mode = args.count("verify") != 0;
	if(!verification_mode) {
		std::cout << std::setw(21) << "Buffer Size / B" << std::setw(21) << "Transfer Time / mus" << std::setw(21) << "Bandwidth / GB/s" << '\n';
	}

	size_t const start_size = args.count("sweep") ? 1 : transfer_size;
	for(auto cur_transfer_size = start_size; cur_transfer_size <= transfer_size; cur_transfer_size *= 2) {
		auto const transfer_method = get_transfer_method(args["method"].as<std::string>(), src_queue, dest_queue, cur_transfer_size);
		transfer_method->operator()(src_buffer, dest_buffer);

		if(verification_mode) {
			verify(transfer_method.get(), src_queue, dest_queue, src_buffer, dest_buffer, num_transfers);
		} else {
			benchmark(transfer_method.get(), src_queue, dest_queue, src_buffer, dest_buffer, num_transfers);
		}
	}
}

namespace {
	void benchmark(Transfer const * const transfer_method, cl_command_queue const src_queue, cl_command_queue const dest_queue, cl_mem const src_buffer, cl_mem const dest_buffer, size_t const num_transfers)
	{
		using namespace std::chrono;

		sync_on_queues({src_queue, dest_queue});
		auto const start_time = steady_clock::now();

		for(size_t i = 0; i < num_transfers; ++i) {
			transfer_method->operator()(src_buffer, dest_buffer);
		}

		sync_on_queues({src_queue, dest_queue});
		auto const end_time = steady_clock::now();
		auto const transfer_duration = duration_cast<microseconds>(end_time - start_time);

		auto const mus = transfer_duration.count() / num_transfers;
		auto const bw = transfer_method->get_size() / static_cast<float>(transfer_duration.count()) * num_transfers / 1e3;
		std::cout.precision(3);
		std::cout << std::setw(21) << transfer_method->get_size() << std::setw(21) << mus << std::setw(21) << bw << '\n';
	}

	bool verify(Transfer const * const transfer_method, cl_command_queue const src_queue, cl_command_queue const dest_queue, cl_mem const src_buffer, cl_mem const dest_buffer, size_t const num_transfers)
	{
		auto const transfer_size = transfer_method->get_size();
		for(size_t i_transfer = 0; i_transfer < num_transfers; ++i_transfer) {
			auto const src_host = random_bytes(transfer_size);
			auto dest_host = random_bytes(transfer_size);
			FAIL_ON_ERR(clEnqueueWriteBuffer(src_queue, src_buffer, CL_TRUE, 0, transfer_size, src_host.data(), 0, nullptr, nullptr));
			FAIL_ON_ERR(clEnqueueWriteBuffer(dest_queue, dest_buffer, CL_TRUE, 0, transfer_size, dest_host.data(), 0, nullptr, nullptr));

			transfer_method->operator()(src_buffer, dest_buffer);

			FAIL_ON_ERR(clEnqueueReadBuffer(dest_queue, dest_buffer, CL_TRUE, 0, transfer_size, dest_host.data(), 0, nullptr, nullptr));

			for(size_t j = 0; j < transfer_size; ++j) {
				if(src_host[j] != dest_host[j]) {
					std::cerr << "Transfer failed for byte " << j << ": 0x" << std::hex << static_cast<unsigned>(src_host[j]) << " != 0x" << static_cast<unsigned>(dest_host[j]) << " (in iteration " << (i_transfer + 1) << ')' << std::endl;
					return false;
				}
			}
		}
		std::cout << "Verification passed for transfer size of " << transfer_size << " B.\n";
		return true;
	}
}

