/*
 * A simple transfer method moving the data via the host.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _PINNED_HPP_
#define _PINNED_HPP_

#include "transfer.hpp"
#include "update.hpp"
#include <vector>

class Pinned : public Transfer {
public:
	Pinned(cl_command_queue src, cl_command_queue dest, size_t bytes);
	virtual ~Pinned();
	virtual void operator()(cl_mem src, cl_mem dest) const override;
private:
	cl_mem pinned_buf;
	void * pinned_mem;
};

class PinnedUpdate : public Update {
public:
	PinnedUpdate(std::vector<cl_command_queue> const & queues, size_t bytes);
	virtual ~PinnedUpdate();
	virtual void operator()(std::vector<cl_mem> const & buffers) const override;
private:
	std::vector<cl_mem> pinned_buffers;
	std::vector<void *> pinned_mems;
};

#endif
