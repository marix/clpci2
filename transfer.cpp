/*
 * A simple transfer method relying on standard opencl methods.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "transfer.hpp"

#include <stdexcept>
#include "buffer_copy.hpp"
#include "pinned.hpp"
#include "dgma.hpp"
#include "dgma_signalled.hpp"

std::unique_ptr<Transfer> get_transfer_method(std::string name, cl_command_queue src, cl_command_queue dest, size_t bytes)
{
	if(name == "buffer_copy") {
		return std::unique_ptr<Transfer>(new BufferCopy(src, dest, bytes));
	} else if(name == "pinned") {
		return std::unique_ptr<Transfer>(new Pinned(src, dest, bytes));
	} else if(name == "dgma") {
		return std::unique_ptr<Transfer>(new DirectGMA(src, dest, bytes));
	} else if(name == "dgma_signalled") {
		return std::unique_ptr<Transfer>(new SignalledDirectGMA(src, dest, bytes));
	}


	throw std::invalid_argument(name + " is not a valid transfer method.");
}
