/*
 * A simple transfer method relying on standard opencl methods.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _TRANSFER_HPP_
#define _TRANSFER_HPP_

#include <CL/cl.h>
#include <memory>

class Transfer {
public:
	virtual void operator()(cl_mem src, cl_mem dest) const = 0;
	size_t get_size() const noexcept { return size; }

protected:
	Transfer(cl_command_queue const src, cl_command_queue const dest, size_t const bytes)
	 : src_queue(src), dest_queue(dest), size(bytes)
	{ };

	cl_command_queue const src_queue;
	cl_command_queue const dest_queue;
	size_t const size;
};

std::unique_ptr<Transfer> get_transfer_method(std::string name, cl_command_queue src, cl_command_queue dest, size_t bytes);

#endif
