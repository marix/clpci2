/*
 * Basic system setup for the benchmarks.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "system.hpp"

#include <iostream>
#include "util.hpp"

namespace {
	std::string get_platform_name(cl_platform_id);
	std::string get_device_name(cl_device_id);
}

System::System(unsigned const selected_platform, std::vector<unsigned> const & selected_devices, size_t const problem_size)
	: queues(), buffers(), problem_size(problem_size)
{
	/*
	 * INIT OpenCL
	 */
	cl_uint num_platforms;
	FAIL_ON_ERR(clGetPlatformIDs(0, nullptr, &num_platforms));
	std::vector<cl_platform_id> platform_ids(num_platforms);
	FAIL_ON_ERR(clGetPlatformIDs(platform_ids.size(), platform_ids.data(), &num_platforms));
	cl_platform_id platform_id = platform_ids.at(selected_platform);
	std::cout << "Platform:            " << get_platform_name(platform_id) << '\n';

	cl_uint num_devices;
	FAIL_ON_ERR(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 0, nullptr, &num_devices));
	std::vector<cl_device_id> device_ids(num_devices);
	FAIL_ON_ERR(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, device_ids.size(), device_ids.data(), &num_devices));
	if(selected_devices.size()) {
		std::vector<cl_device_id> tmp(selected_devices.size());
		for(size_t i = 0; i < selected_devices.size(); ++i) {
			tmp[i] = device_ids.at(selected_devices[i]);
			std::cout << "Device " << i << ": " << get_device_name(tmp[i]) << " [" << selected_devices[i] << "]\n";
		}
		device_ids = std::move(tmp);
	} else {
		for(size_t i = 0; i < device_ids.size(); ++i) {
			std::cout << "Device " << i << ": " << get_device_name(device_ids[i]) << " [" << i << "]\n";
		}
	}

	cl_int clerr;
	cl_context_properties const context_props[] = {CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id, 0};
	context = clCreateContext(context_props, device_ids.size(), device_ids.data(), nullptr, nullptr, &clerr);
	FAIL_ON_ERR(clerr);

	for(auto const device: device_ids) {
		queues.push_back(clCreateCommandQueue(context, device, 0, &clerr));
	}

	/*
	 * Setup buffers
	 */
	for(auto const queue: queues) {
		auto const buffer = clCreateBuffer(context, 0, problem_size, nullptr, &clerr);
		FAIL_ON_ERR(clerr);
		FAIL_ON_ERR(clEnqueueMigrateMemObjects(queue, 1, &buffer, CL_MIGRATE_MEM_OBJECT_CONTENT_UNDEFINED, 0, nullptr, nullptr));
		buffers.push_back(buffer);
	}
	sync_on_queues(queues);
}

System::~System()
{
	/*
	 * Cleanup
	 */
	for(auto const buffer: buffers) {
		clReleaseMemObject(buffer);
	}
	for(auto const queue: queues) {
		clReleaseCommandQueue(queue);
	}
	clReleaseContext(context);
}


namespace {
	std::string get_platform_name(cl_platform_id const platform)
	{
		size_t res_size;
		FAIL_ON_ERR(clGetPlatformInfo(platform, CL_PLATFORM_NAME, 0, nullptr, &res_size));
		std::vector<char> result(res_size);
		FAIL_ON_ERR(clGetPlatformInfo(platform, CL_PLATFORM_NAME, result.size(), result.data(), &res_size));
		return std::string(result.data());
	}

	std::string get_device_name(cl_device_id const device)
	{
		size_t res_size;
		FAIL_ON_ERR(clGetDeviceInfo(device, CL_DEVICE_NAME, 0, nullptr, &res_size));
		std::vector<char> result(res_size);
		FAIL_ON_ERR(clGetDeviceInfo(device, CL_DEVICE_NAME, result.size(), result.data(), &res_size));
		return std::string(result.data());
	}
}

void sync_on_queues(std::vector<cl_command_queue> const & queues)
{
	auto const num_queues = queues.size();
	std::vector<cl_event> events(num_queues);
	for(size_t i = 0u; i < num_queues; ++i) {
		FAIL_ON_ERR(clEnqueueMarkerWithWaitList(queues[i], 0, nullptr, &events[i]));
	}

	FAIL_ON_ERR(clWaitForEvents(events.size(), events.data()));
	for(auto event: events) {
		clReleaseEvent(event);
	}
}

