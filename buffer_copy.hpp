/*
 * A simple transfer method relying on standard opencl methods.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _BUFFER_COPY_HPP_
#define _BUFFER_COPY_HPP_

#include "transfer.hpp"
#include <vector>

class BufferCopy : public Transfer {
public:
	BufferCopy(cl_command_queue src, cl_command_queue dest, size_t bytes);
	virtual ~BufferCopy();
	virtual void operator()(cl_mem src, cl_mem dest) const override;
private:
	cl_kernel _dummy;
	void touch(cl_command_queue, cl_mem, std::vector<cl_event> = {}, cl_event * = nullptr) const;
	mutable cl_event copy_event = 0;
};

#endif

