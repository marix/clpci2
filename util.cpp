/*
 * Some utilities.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "util.hpp"

#include <sstream>
#include <stdexcept>
#include <random>

void FAIL_ON_ERR(cl_int err)
{
	if(err != CL_SUCCESS) {
		std::stringstream tmp;
		tmp << "Ran into OpenCL error " << err;
		throw std::runtime_error(tmp.str());
	}
}

std::vector<unsigned char> random_bytes(size_t num_bytes)
{
	std::vector<unsigned char> result(num_bytes);
	static std::independent_bits_engine<std::default_random_engine,8,unsigned char> generator(13);
	// I don't care about the distribution -> use generator directly
	for(size_t i = 0; i < num_bytes; ++i) {
		result[i] = generator();
	}
	return result;
}
