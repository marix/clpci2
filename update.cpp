/*
 * Execute buffer updates between neighbouring devices.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
*/

#include <chrono>
#include <iostream>
#include <iomanip>
#include <vector>
#include <boost/program_options.hpp>
#include <CL/cl.h>
#include "update.hpp"
#include "buffer_copy.hpp"
#include "util.hpp"
#include "system.hpp"

namespace {
	void benchmark(Update const * update_method, std::vector<cl_command_queue> const & queues, std::vector<cl_mem> const & buffers, size_t num_transfers);
	bool verify(Update const * update_method, std::vector<cl_command_queue> const & queues, std::vector<cl_mem> const & buffers, size_t num_transfers);
}

int main(int ac, const char** av)
{
	namespace po = boost::program_options;

	/*
	 * HANDLE COMMAND LINE
	 */
	po::options_description desc("Allowed Options");
	desc.add_options()
		("help,h", "produce help message")
		("platform,p", po::value<unsigned>()->default_value(0), "Select platform")
		("device", po::value<std::vector<unsigned>>(), "The devices to utilize.")
		("size,s", po::value<size_t>()->default_value(4*1024*1024), "Transfer size in bytes")
		("transfers,n", po::value<size_t>()->default_value(1000), "Number of transfers to perform")
		("verify,c", "Verify transfer instead of benchmarking")
		("method,m", po::value<std::string>()->default_value("buffer_copy"), "The copy method to use")
		("sweep", "Sweep through the memory sizes")
	;

	po::positional_options_description p_desc;
	p_desc.add("device", -1);

	po::variables_map args;
	po::store(po::command_line_parser(ac, av).options(desc).positional(p_desc).run(), args);
	po::notify(args);

	if(args.count("help")) {
		std::cout << desc << '\n';
		return 1;
	}

	auto const buffer_size = args["size"].as<size_t>();
	std::cout << "Buffer Size (B):     " << buffer_size << '\n';
	auto const num_transfers = args["transfers"].as<size_t>();
	std::cout << "Number of Transfers: " << num_transfers << '\n';

	std::vector<unsigned> device_selection = args.count("device") ? args["device"].as<std::vector<unsigned>>() : std::vector<unsigned>{};
	System const system(args["platform"].as<unsigned>(), device_selection, buffer_size);
	auto const & queues = system.get_queues();
	auto const & buffers = system.get_buffers();

	/*
	 * PERFORM BENCHMARK / VERIFICATION
	 */
	auto const verification_mode = args.count("verify") != 0;
	if(!verification_mode) {
		std::cout << std::setw(21) << "Buffer Size / B" << std::setw(21) << "Transfer Time / mus" << std::setw(21) << "Bandwidth / GB/s" << '\n';
	}

	size_t const start_size = args.count("sweep") ? 2 : buffer_size;
	for(auto cur_buffer_size = start_size; cur_buffer_size <= buffer_size; cur_buffer_size *= 2) {
		auto const update_method = get_update_method(args["method"].as<std::string>(), queues, cur_buffer_size);
		update_method->operator()(buffers);

		if(verification_mode) {
			verify(update_method.get(), queues, buffers, num_transfers);
		} else {
			benchmark(update_method.get(), queues, buffers, num_transfers);
		}
	}
}

namespace {
	void benchmark(Update const * update_method, std::vector<cl_command_queue> const & queues, std::vector<cl_mem> const & buffers, size_t num_transfers)
	{
		using namespace std::chrono;

		sync_on_queues(queues);
		auto const start_time = steady_clock::now();

		for(size_t i = 0; i < num_transfers; ++i) {
			update_method->operator()(buffers);
		}

		sync_on_queues(queues);
		auto const end_time = steady_clock::now();
		auto const transfer_duration = duration_cast<microseconds>(end_time - start_time);

		auto const mus = transfer_duration.count() / num_transfers;
		auto const bw = update_method->get_size() / 2 * queues.size() / static_cast<float>(transfer_duration.count()) * num_transfers / 1e3;
		std::cout.precision(3);
		std::cout << std::setw(21) << update_method->get_size() << std::setw(21) << mus << std::setw(21) << bw << '\n';
	}

	bool verify(Update const * update_method, std::vector<cl_command_queue> const & queues, std::vector<cl_mem> const & buffers, size_t num_transfers)
	{
		auto const buffer_size = update_method->get_size();
		auto const num_devices = queues.size();
		auto const half_size = buffer_size / 2;
		for(size_t i_transfer = 0; i_transfer < num_transfers; ++i_transfer) {
			std::vector<std::vector<unsigned char>> host(num_devices);
			for(size_t i = 0; i < num_devices; ++i) {
				host[i] = random_bytes(buffer_size);
				FAIL_ON_ERR(clEnqueueWriteBuffer(queues[i], buffers[i], CL_TRUE, 0, buffer_size, host[i].data(), 0, nullptr, nullptr));
			}

			update_method->operator()(buffers);

			for(size_t i = 0; i < num_devices; ++i) {
				FAIL_ON_ERR(clEnqueueReadBuffer(queues[i], buffers[i], CL_TRUE, 0, half_size, host[i].data(), 0, nullptr, nullptr));
			}

			for(size_t dest_dev = 0; dest_dev < num_devices; ++dest_dev) {
				auto const src_dev = get_lower_neighbour(dest_dev, num_devices);
				auto const & src_host = host[src_dev];
				auto const & dest_host = host[dest_dev];
				for(size_t j = 0; j < half_size; ++j) {
					if(src_host[j + half_size] != dest_host[j]) {
						std::cerr << "Transfer to device " << dest_dev << " failed for byte " << j << ": 0x" << std::hex << static_cast<unsigned>(src_host[j]) << " != 0x" << static_cast<unsigned>(dest_host[j]) << " (in iteration " << (i_transfer + 1) << ", source device: " << src_dev << ')' << std::endl;
						return false;
					}
				}
			}
		}
		std::cout << "Verification passed for problem size of " << buffer_size << " B.\n";
		return true;
	}
}
