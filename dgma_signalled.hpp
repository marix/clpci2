/*
 * A simple transfer method moving the data via the host.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _SIGNALLED_DGMA_HPP_
#define _SIGNALLED_DGMA_HPP_

#include "transfer.hpp"
#include <vector>
#include <CL/cl_ext.h>

class SignalledDirectGMA : public Transfer {
public:
	SignalledDirectGMA(cl_command_queue src, cl_command_queue dest, size_t bytes);
	virtual ~SignalledDirectGMA();
	virtual void operator()(cl_mem src, cl_mem dest) const override;
private:
	/** the cache buffer on the dest device that can be accessed via bus */
	cl_mem bus_addressable_buffer = 0;
	/** the buffer on the source device, which is actually backed by the buffer bus_visible_buffer */
	cl_mem shadow_buffer = 0;
	cl_ulong marker_offset = 0;
	mutable cl_uint marker_value = 0;
	// OpenCL extension functions required for SignalledDirectGMA
	clEnqueueWaitSignalAMD_fn clEnqueueWaitSignalAMD = nullptr;
	clEnqueueWriteSignalAMD_fn clEnqueueWriteSignalAMD = nullptr;
	clEnqueueMakeBuffersResidentAMD_fn clEnqueueMakeBuffersResidentAMD = nullptr;
};

#endif
