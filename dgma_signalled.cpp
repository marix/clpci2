/*
 * A transfer method using AMDs SignalledDirectGMA.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "dgma_signalled.hpp"

#include <stdexcept>
#include "util.hpp"

SignalledDirectGMA::SignalledDirectGMA(cl_command_queue const src, cl_command_queue const dest, size_t const bytes)
 : Transfer(src, dest, bytes) {

	cl_context context;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
	cl_device_id src_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_DEVICE, sizeof(cl_device_id), &src_device, nullptr));
	cl_device_id dest_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(dest, CL_QUEUE_DEVICE, sizeof(cl_device_id), &dest_device, nullptr));
	cl_platform_id platform;
	FAIL_ON_ERR(clGetDeviceInfo(dest_device, CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &platform, nullptr));

	clEnqueueWaitSignalAMD = reinterpret_cast<clEnqueueWaitSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWaitSignalAMD"));
	clEnqueueWriteSignalAMD = reinterpret_cast<clEnqueueWriteSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWriteSignalAMD"));
	clEnqueueMakeBuffersResidentAMD = reinterpret_cast<clEnqueueMakeBuffersResidentAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueMakeBuffersResidentAMD"));
	if(!clEnqueueWaitSignalAMD || !clEnqueueWriteSignalAMD || !clEnqueueMakeBuffersResidentAMD) {
		throw std::runtime_error("Failed to resolve SignalledDirectGMA functions. Does the current platform support SignalledDirectGMA?");
	}

	// create bus addressable buffer on the destination device
	cl_int err;
	bus_addressable_buffer = clCreateBuffer(context, CL_MEM_BUS_ADDRESSABLE_AMD, size, nullptr, &err);
	FAIL_ON_ERR(err);
	cl_bus_address_amd bus_addressable_buffer_address;
	FAIL_ON_ERR(clEnqueueMakeBuffersResidentAMD(dest, 1, &bus_addressable_buffer, CL_TRUE, &bus_addressable_buffer_address, 0, nullptr, nullptr));

	// create the shadow buffer on the source device that is backed by the bus_addressable_buffer's memory
	// the marker address must be aligned to 4K, for some reason this must be ensured by hand
	marker_offset = bus_addressable_buffer_address.marker_bus_address & 0xfff;
	bus_addressable_buffer_address.marker_bus_address &= ~0xfff;
	shadow_buffer = clCreateBuffer(context, CL_MEM_EXTERNAL_PHYSICAL_AMD, size, &bus_addressable_buffer_address, &err);
	FAIL_ON_ERR(err);
	FAIL_ON_ERR(clEnqueueMigrateMemObjects(src, 1, &shadow_buffer, 0, 0, nullptr, nullptr));
	FAIL_ON_ERR(clFinish(src));

}

SignalledDirectGMA::~SignalledDirectGMA()
{
	// todo release locked memory...
	clReleaseMemObject(shadow_buffer);
	clReleaseMemObject(bus_addressable_buffer);
}

void SignalledDirectGMA::operator()(cl_mem const src, cl_mem const dest) const
{
	++marker_value;

	FAIL_ON_ERR(clEnqueueCopyBuffer(src_queue, src, shadow_buffer, 0, 0, size, 0, nullptr, nullptr));
	FAIL_ON_ERR(clEnqueueWriteSignalAMD(src_queue, shadow_buffer, marker_value, marker_offset, 0, nullptr, nullptr));
	clFlush(src_queue);

	FAIL_ON_ERR(clEnqueueWaitSignalAMD(dest_queue, bus_addressable_buffer, marker_value, 0, nullptr, nullptr));
	cl_event dest_copy;
	FAIL_ON_ERR(clEnqueueCopyBuffer(dest_queue, bus_addressable_buffer, dest, 0, 0, size, 0, nullptr, &dest_copy));
	clFlush(dest_queue);

	FAIL_ON_ERR(clEnqueueBarrierWithWaitList(src_queue, 1, &dest_copy, nullptr));

	clReleaseEvent(dest_copy);
}
