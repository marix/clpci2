/*
 * Some utilities.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#ifndef _UTIL_HPP_
#define _UTIL_HPP_

#include <CL/cl.h>
#include <vector>

void FAIL_ON_ERR(cl_int);
std::vector<unsigned char> random_bytes(size_t);

inline size_t get_lower_neighbour(size_t idx, size_t max) {
	return (idx > 0) ? idx - 1 : max - 1;
}

inline size_t get_upper_neighbour(size_t idx, size_t max) {
	size_t res = idx + 1;
	return (res < max) ? res : 0;
}

#endif
