/*
 * A transfer method using AMDs DirectGMA.
 *
 * This file is part of CLPCI2.
 *
 * CLPCI2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CLPCI2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CLPCI2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (c) 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
 */

#include "dgma.hpp"

#include <stdexcept>
#include "util.hpp"

DirectGMA::DirectGMA(cl_command_queue const src, cl_command_queue const dest, size_t const bytes)
 : Transfer(src, dest, bytes) {

	cl_context context;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
	cl_device_id src_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(src, CL_QUEUE_DEVICE, sizeof(cl_device_id), &src_device, nullptr));
	cl_device_id dest_device;
	FAIL_ON_ERR(clGetCommandQueueInfo(dest, CL_QUEUE_DEVICE, sizeof(cl_device_id), &dest_device, nullptr));
	cl_platform_id platform;
	FAIL_ON_ERR(clGetDeviceInfo(dest_device, CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &platform, nullptr));

	clEnqueueWaitSignalAMD = reinterpret_cast<clEnqueueWaitSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWaitSignalAMD"));
	clEnqueueWriteSignalAMD = reinterpret_cast<clEnqueueWriteSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWriteSignalAMD"));
	clEnqueueMakeBuffersResidentAMD = reinterpret_cast<clEnqueueMakeBuffersResidentAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueMakeBuffersResidentAMD"));
	if(!clEnqueueWaitSignalAMD || !clEnqueueWriteSignalAMD || !clEnqueueMakeBuffersResidentAMD) {
		throw std::runtime_error("Failed to resolve DirectGMA functions. Does the current platform support DirectGMA?");
	}

	// create bus addressable buffer on the destination device
	cl_int err;
	bus_addressable_buffer = clCreateBuffer(context, CL_MEM_BUS_ADDRESSABLE_AMD, size, nullptr, &err);
	FAIL_ON_ERR(err);
	FAIL_ON_ERR(clEnqueueMakeBuffersResidentAMD(dest, 1, &bus_addressable_buffer, CL_TRUE, &bus_addressable_buffer_address, 0, nullptr, nullptr));

	// create the shadow buffer on the source device that is backed by the bus_addressable_buffer's memory
	shadow_buffer = clCreateBuffer(context, CL_MEM_EXTERNAL_PHYSICAL_AMD, size, &bus_addressable_buffer_address, &err);
	FAIL_ON_ERR(err);
	FAIL_ON_ERR(clEnqueueMigrateMemObjects(src, 1, &shadow_buffer, 0, 0, nullptr, nullptr));
	FAIL_ON_ERR(clFinish(src));
}

DirectGMA::~DirectGMA()
{
	// todo release locked memory...
	clReleaseMemObject(shadow_buffer);
	clReleaseMemObject(bus_addressable_buffer);
}

void DirectGMA::operator()(cl_mem const src, cl_mem const dest) const
{
	cl_event src_copy;
	FAIL_ON_ERR(clEnqueueCopyBuffer(src_queue, src, shadow_buffer, 0, 0, size, 0, nullptr, &src_copy));

	cl_event dest_copy;
	FAIL_ON_ERR(clEnqueueCopyBuffer(dest_queue, bus_addressable_buffer, dest, 0, 0, size, 1, &src_copy, &dest_copy));

	FAIL_ON_ERR(clEnqueueBarrierWithWaitList(src_queue, 1, &dest_copy, nullptr));
	clFlush(src_queue);
	clFlush(dest_queue);

	clReleaseEvent(src_copy);
	clReleaseEvent(dest_copy);
}

DirectGMAUpdate::DirectGMAUpdate(std::vector<cl_command_queue> const & queues, size_t const bytes)
 : Update(queues, bytes), bus_addressable_buffers(queues.size()), bus_addressable_buffer_addresses(queues.size()),
   shadow_buffers(queues.size()) {

	auto const queue1 = queues.at(0);

	cl_context context;
	FAIL_ON_ERR(clGetCommandQueueInfo(queue1, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, nullptr));
	cl_device_id device1;
	FAIL_ON_ERR(clGetCommandQueueInfo(queue1, CL_QUEUE_DEVICE, sizeof(cl_device_id), &device1, nullptr));
	cl_platform_id platform;
	FAIL_ON_ERR(clGetDeviceInfo(device1, CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &platform, nullptr));

	clEnqueueWaitSignalAMD = reinterpret_cast<clEnqueueWaitSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWaitSignalAMD"));
	clEnqueueWriteSignalAMD = reinterpret_cast<clEnqueueWriteSignalAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueWriteSignalAMD"));
	clEnqueueMakeBuffersResidentAMD = reinterpret_cast<clEnqueueMakeBuffersResidentAMD_fn>(clGetExtensionFunctionAddressForPlatform(platform, "clEnqueueMakeBuffersResidentAMD"));
	if(!clEnqueueWaitSignalAMD || !clEnqueueWriteSignalAMD || !clEnqueueMakeBuffersResidentAMD) {
		throw std::runtime_error("Failed to resolve DirectGMA functions. Does the current platform support DirectGMA?");
	}

	auto const transfer_size = size / 2;

	for(size_t i = 0; i < queues.size(); ++i) {
		auto const lower = get_lower_neighbour(i, queues.size());

		// create bus addressable buffer on the destination device
		cl_int err;
		bus_addressable_buffers[i] = clCreateBuffer(context, CL_MEM_BUS_ADDRESSABLE_AMD, transfer_size, nullptr, &err);
		FAIL_ON_ERR(err);
		FAIL_ON_ERR(clEnqueueMakeBuffersResidentAMD(queues[i], 1, &bus_addressable_buffers[i], CL_TRUE, &bus_addressable_buffer_addresses[i], 0, nullptr, nullptr));

		// create the shadow buffer on the source device that is backed by the bus_addressable_buffer's memory
		shadow_buffers[lower] = clCreateBuffer(context, CL_MEM_EXTERNAL_PHYSICAL_AMD, transfer_size, &bus_addressable_buffer_addresses[i], &err);
		FAIL_ON_ERR(err);
		FAIL_ON_ERR(clEnqueueMigrateMemObjects(queues[lower], 1, &shadow_buffers[lower], 0, 0, nullptr, nullptr));
		FAIL_ON_ERR(clFinish(queues[lower]));
	}
}

DirectGMAUpdate::~DirectGMAUpdate()
{
	for(auto const buffer: shadow_buffers) {
		clReleaseMemObject(buffer);
	}
	for(auto const buffer: bus_addressable_buffers) {
		clReleaseMemObject(buffer);
	}
}

void DirectGMAUpdate::operator()(std::vector<cl_mem> const & buffers) const
{
	auto const num_devices = queues.size();
	std::vector<cl_event> src_copies(num_devices);
	std::vector<cl_event> dest_copies(num_devices);
	auto const transfer_size = size / 2;

	// write from src to transfer buffers
	for(size_t i_src = 0; i_src < num_devices; ++i_src) {
		FAIL_ON_ERR(clEnqueueCopyBuffer(queues[i_src], buffers[i_src], shadow_buffers[i_src], transfer_size, 0, transfer_size, 0, nullptr, &src_copies[i_src]));
	}

	// write from transfer to src buffers
	for(size_t i_dest = 0; i_dest < num_devices; ++i_dest) {
		auto const i_src = get_lower_neighbour(i_dest, num_devices);
		FAIL_ON_ERR(clEnqueueCopyBuffer(queues[i_dest], bus_addressable_buffers[i_dest], buffers[i_dest], 0, 0, transfer_size, 1, &src_copies[i_src], &dest_copies[i_dest]));
	}

	// ensure buffer is not written to by the sending device until finished
	for(size_t i_dest = 0; i_dest < num_devices; ++i_dest) {
		auto const i_src = get_lower_neighbour(i_dest, num_devices);
		FAIL_ON_ERR(clEnqueueBarrierWithWaitList(queues[i_src], 1, &dest_copies[i_dest], nullptr));
	}

	// flush the queues
	for(auto const queue: queues) {
		clFlush(queue);
	}

	// clean up our resources (events)
	for(auto const event: src_copies) {
		clReleaseEvent(event);
	}
	for(auto const event: dest_copies) {
		clReleaseEvent(event);
	}
}
