CLPCI2
======

CLPCI2 is a utility to measure the achievable bandwidth in transfers between device and host, usually occuring via PCI.
It is the successor of clPCI.
While clPCI allowed fast prototyping of transfer methods it was limited to the subset of OpenCL supported by pyopencl.
Based on the C API of OpenCL, CLPCI2 now allows to utilize non-standard features such as AMD's DirectGMA.

License
=======

CLPCI2 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CLPCI2 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with CLPCI2.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

&copy; 2013 Matthias Bach <bach@compeng.uni-frankfurt.de>
